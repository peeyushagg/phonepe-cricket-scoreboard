# How to play #
The program is an interactive shell driven game. No external dependencies required to run the program except Python3

Go to the path same as that of the README.txt file [cricket/] and run the following command

`python3 runner.py`

Provide the appropriate inputs as the command prompt asks for.