from entities.team import Team
from entities.match import Match



if __name__=="__main__":
    number_of_team_members = int(input("Enter no. of players in each team: "))
    number_of_overs = int(input("Enter no. of overs in match: "))

    team1_name, team2_name = "Team1", "Team2"
    team1_batting_order, team2_batting_order = [], []

    print("Enter batting order of Team1")
    for _ in range(number_of_team_members):
        name = input()
        team1_batting_order.append(name)

    print("Enter batting order of Team1")
    for _ in range(number_of_team_members):
        name = input()
        team2_batting_order.append(name)

    print("Let's Play!!!\n\n\n")

    team1 = Team(team1_name, team1_batting_order)
    team2 = Team(team2_name, team2_batting_order)

    match = Match(team1, team2, number_of_overs)
    match.play_match()
